# About

Manage restartable jobs with hoomd-blue.

**Requires the latest `master` version of `hoomd-blue`.**

# Setup

Make sure that you are installing with the python version, that is used with hoomd.

    $ python setup install.py --user

# Usage

## Restartable Jobs

To create restartable jobs use the `run_upto` wrapper provided with this package.

    from hoomd_script import *
    from hoomd_restartable import run_upto, attempt_restart
    # Make sure to import this after hoomd_script

    try:
      system = attempt_restart('my_job')
    except FileNotFoundError:
      system = init.create_random(N = 100, phi_p = 0.05)

    [..]

    run_upto(
      step = num_steps,
      restart_id = 'my_job',
      restart_period = 100)

## Manage wallclock time

If you want to manage your maximum wall clock time, add the following commands

    from hoomd_script import *
    from hoomd_restartable import run_upto
    from hoomd_restartable import walltime
    walltime.set_max_walltime(30 * 60) # in seconds

    [..]

    run_upto(
      step  = num_steps,
      check_walltime = True,
      callback_period = 200)

You can still add your own callback, but the remaining wall clock time will be checked every `callback_period` steps.
When the wall clock time comes close to the maximum walltime the simulation will be aborted.

**You can use both features combined.**