from setuptools import setup, find_packages

setup(
    name = 'hoomd-blue-restartable',
    version = '0.1',
    package_dir = {'': 'src'},
    packages = find_packages('src'),

    author = 'Carl Simon Adorf',
    author_email = 'csadorf@umich.edu',
    description = "Use this package to manage restartable jobs with hoomd-blue.",
    keywords = 'hoomd hoomd-blue restartable glotzer md mc molecular-dynamics monte-carlo',

    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Topic :: Scientific/Engineering :: Physics",
        ],
)
