from . import walltime
from .restart import run_upto, attempt_restart

def use_global():
    import hoomd_script as hoomd
    hoomd.run_upto = run_upto
