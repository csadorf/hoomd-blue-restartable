import logging
logger = logging.getLogger(__name__)

STANDARD_TOLERANCE = 0.05
MAX_WALLTIME = None

import time

time_start = time.time()
clock_start = time.clock()

class OutOfTimeError(EnvironmentError):
    pass

def walltime():
    return time.time() - time_start

def wallclock():
    return time.clock() - clock_start

def within_limit(max_walltime = None, tolerance = None):
    max_walltime = max_walltime or MAX_WALLTIME
    if max_walltime is None:
        raise ValueError("No walltime set.")
    if tolerance is None:
        tolerance = STANDARD_TOLERANCE
    ratio = walltime() / max_walltime
    msg = "Checking walltime, process = {:.2%} of {:.2}h."
    logger.debug(msg.format(ratio, max_walltime/3600))
    return ratio < 1.0-tolerance

def exit_by(max_walltime, tolerance = None):
    if not within_limit(max_walltime, tolerance):
        msg = "Reached end of walltime {:.2}h, exiting."
        logger.warning(msg.format(max_walltime/3600))
        import sys
        sys.exit(2)

def raise_by(max_walltime, tolerance = None):
    if not within_limit(max_walltime, tolerance):
        raise OutOfTimeError(walltime())

def set_max_walltime(max_walltime):
    global MAX_WALLTIME
    MAX_WALLTIME = max_walltime
    msg = "Maximum walltime set to {:.2}h."
    logger.info(msg.format(max_walltime/3600))

def get_max_walltime():
    return int(MAX_WALLTIME)

def within_limit_or_exit(max_walltime = None):
    exit_by(max_walltime)

def within_limit_or_raise(max_walltime = None):
    raise_by(max_walltime)
