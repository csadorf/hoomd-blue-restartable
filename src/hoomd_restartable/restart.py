import logging
logger = logging.getLogger(__name__)

from hoomd_script import run_upto as hoomd_runupto

def get_fn_restart(restart_id):
    return '{}.restart.xml'.format(restart_id)

def attempt_restart(restart_id):
    import os.path
    import hoomd_script as hoomd
    fn_restart = get_fn_restart(restart_id)
    try:
        if os.path.isfile(fn_restart):
            system = hoomd.init.read_xml(filename = fn_restart)
        else:
            msg = "Did not find restart file with name '{}'."
            logger.debug(msg.format(fn_restart))
            raise FileNotFoundError()
    except FileNotFoundError:
        raise
    except Exception:
        logger.debug("Unable to restart from '{}'.".format(fn_restart))
        raise
    else:
        logger.info("Restarted from file '{}'.".format(fn_restart))
        return system

def make_write_restart_file_callback(fn_restart):
    fn_tmp = fn_restart + '.tmp'
    def restart_callback(step):
        import hoomd_script as hoomd
        import os
        if hoomd.comm.get_rank() == 0:
            logger.info("Saving restart file {} at step {}.".format(fn_restart, step))
        hoomd.dump.xml(fn_tmp, vis = True, orientation = True)
        if hoomd.comm.get_rank() == 0:
            try:
                os.rename(fn_tmp, fn_restart)
            except Exception as error:
                logger.error(error)
                raise
            else: logger.debug("Renamed '{}' -> '{}'.".format(fn_tmp, fn_restart))
    return restart_callback

def run_upto_with_restart(  step,
                            restart_id = None, restart_period = None, restart_phase = None,
                            *args, **kwargs):
    import hoomd_script as hoomd
    if restart_id is None:
        return hoomd_runupto(step, *args, **kwargs)
    elif restart_id is None and restart_period is not None:
        raise ValueError("No restart_id set.")
    elif restart_id is not None and restart_period is None:
        raise ValueError("You need to set a restart_period.")
    else:
        if restart_phase is None:
            restart_phase = restart_period
        fn_restart = get_fn_restart(restart_id)
        restart_callback = make_write_restart_file_callback(fn_restart)
        a_cb = hoomd.analyze.callback(
            callback = restart_callback,
            period = restart_period,
            phase = restart_phase)
        result = hoomd_runupto(
            step = step,
            * args, ** kwargs)
        if restart_phase > 0:
            if hoomd.get_step() + restart_phase % hoomd.get_step() == 0:
                restart_callback(step)
        else:
            restart_callback(step)
        a_cb.disable()
        return result

def run_upto(   step, callback = None, callback_period = 0,
                check_walltime = False, *args, **kwargs):
    if check_walltime:
        try:
            from . import walltime
        except ImportError:
            msg = "Missing plugin 'walltime'."
            raise EnvironmentError(msg)
        if callback_period == 0:
            msg = "Set a callback_period for walltime check."
            raise ValueError(msg)
        if callback is None:
            def callback_(num_steps):
                if not walltime.within_limit():
                    return -1
        else:
            def callback_(num_steps):
                cb_result = callback(num_steps)
                if cb_result < 0:
                    return cb_result
                if not walltime.within_limit():
                    return -1
                else:
                    return cb_result
    elif callback is None:
        return run_upto_with_restart(step=step, *args, **kwargs)
    else:
        callback_ = callback
    return run_upto_with_restart(
        step = step,
        callback = callback_,
        callback_period = callback_period,
        * args, ** kwargs)
